package com.it.sendmail.Controller;



import com.it.sendmail.Dao.RespBean;

import com.it.sendmail.Dao.User;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.TemplateEngine;

import javax.annotation.Resource;
import java.util.Date;

/**
 * (TUser)表控制层
 */
@RestController
@RequestMapping("/tUser")
public class UserController {

   @Resource
    private RabbitTemplate rabbitTemplate;

    @PostMapping("/mails")
    public RespBean doMail(@RequestBody User user){
        System.out.println("数据: " + user);
        if (user != null){
            rabbitTemplate.convertAndSend("mq.email",user);
            return RespBean.success("查询成功",user);
        }
        return RespBean.error("发送失败！");
    }

}
