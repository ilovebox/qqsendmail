package com.it.sendmail.Config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    /**
     * 创建一个队列
     * @return
     */
    @Bean
    public Queue mailQueue() {
        return new Queue("mq.email");//队列名称
    }
}
