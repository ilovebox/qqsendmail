package com.it.sendmail.Utils;

import com.it.sendmail.Dao.MailBean;
import com.it.sendmail.Dao.RespBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

@Component
public class CheckCode {

    @Resource
    StringRedisTemplate stringRedisTemplate;

    public  void checkCode(@RequestParam("code")  String code){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        String s = stringRedisTemplate.opsForValue().get(code);
        if(!StringUtils.isEmpty(s)){
            if(code==s)
            stringRedisTemplate.delete(code);
        }else{
            RespBean.error("发送失败！");
        }

    }

}
